package com.afkl.cases.df;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URI;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
public class FareResource {
	
	@Autowired private RestTemplate restTemplate;
	
	@GetMapping("/health")
	public String getHealth() {
		return "Server is up and running";
	}
	
	@CrossOrigin(origins = "http://localhost:3000")
	@GetMapping("/airports")
	public String airports() throws Exception{
		String methodUrl = "http://localhost:9091/airports?access_token=" + getTokenCurl();
		return restTemplate.getForObject(URI.create(methodUrl), String.class);
	}
	
	@CrossOrigin(origins = "http://localhost:3000")
	@GetMapping("/fare/{origin}/{destination}")
	public String fare(@PathVariable String origin, @PathVariable String destination, @RequestParam(defaultValue = "EUR") String currencyCode) throws Exception{
		String methodUrl = "http://localhost:9091/fares/" + origin + "/" + destination + "?access_token=" + getTokenCurl();
		return restTemplate.getForObject(URI.create(methodUrl), String.class);
	}
	
	private String getTokenCurl() throws IOException {
		String command = "curl travel-api-client:psw@localhost:9091/oauth/token -dgrant_type=client_credentials";
		Process p;
		ProcessBuilder process = new ProcessBuilder(command.split(" "));
		String result = new String();
		
		try {
			p = process.start();
			BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
			StringBuilder builder = new StringBuilder();
			String line = null;
			while ((line = reader.readLine()) != null) {
				builder.append(line);
				builder.append(System.getProperty("line.separator"));
			}
			System.out.print(builder.toString());
			result =  builder.toString();

		} catch (IOException e) {
			System.out.print("error");
			e.printStackTrace();
		}
		return result.split(",")[0].split(":")[1].replaceAll("\"", "");
	}
}
