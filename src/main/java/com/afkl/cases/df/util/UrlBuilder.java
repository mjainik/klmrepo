package com.afkl.cases.df.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class UrlBuilder {
	
	public static String of(String basePath, String method) throws IOException {
		return basePath + method + "?access_token=" + getTokenCurl();
	}
	
	public static String of(String basePath, String method, String origin, String destination) throws IOException {
		return basePath + method + origin + "/" + destination + "?access_token=" + getTokenCurl();
	}

	private static String getTokenCurl() throws IOException {
		String command = "curl travel-api-client:psw@localhost:8080/oauth/token -dgrant_type=client_credentials";
		Process p;
		ProcessBuilder process = new ProcessBuilder(command.split(" "));
		String result = new String();
		
		try {
			p = process.start();
			BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
			StringBuilder builder = new StringBuilder();
			String line = null;
			while ((line = reader.readLine()) != null) {
				builder.append(line);
				builder.append(System.getProperty("line.separator"));
			}
			System.out.print(builder.toString());
			result =  builder.toString();

		} catch (IOException e) {
			System.out.print("error");
			e.printStackTrace();
		}
		return result.split(",")[0].split(":")[1].replaceAll("\"", "");
	}
}
